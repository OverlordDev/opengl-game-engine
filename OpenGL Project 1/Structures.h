#ifndef STRUCTURES_H
#define STRUCTURES_H
typedef enum {
	GF_NORMAL = 0,
	GF_TILTINGX = 1,
	GF_TILTINGY__ = 2,
	GF_TILTINGXY = 3,
	GF_ADDITIONAL_HEADER_FIELD = 8,
	GF_UNKNONWN = 16
} E_GLOBALFLAGS;
struct TheFloats
{
public:
	TheFloats()
	{

	}

	glm::vec3 VertexBoxMin;
	glm::vec3 VertexBoxMax;
	float VertexBoxRadius;

	glm::vec3 BoundingBoxMax;
	glm::vec3 BoundingBoxMin;
	float BoundingBoxRadius;

};
class M2Vertex
{
public:
	M2Vertex(float x, float y, float z, __int8 bwx, __int8 bwy, __int8 bwz, __int8 bww, __int8 bix, __int8 biy, __int8 biz, __int8 biw, float nx, float ny, float nz, float tx, float ty, float unkx, float unky)
	{
		Position = glm::vec3(x, y, z);
		BoneWeight = glm::u16vec4(bwx, bwy, bwz, bww);
		BoneIndices = glm::u16vec4(bix, biy, biz, biw);
		Normals = glm::vec3(nx, ny, nz);
		TextureCoords = glm::vec2(tx, ty);
		Unknown = glm::vec2(unkx, unky);
	}
	M2Vertex()
	{

	}
	glm::vec3 Position;
	glm::u16vec4 BoneWeight;
	glm::u16vec4 BoneIndices;
	glm::vec3 Normals;
	glm::vec2 TextureCoords;
	glm::vec2 Unknown;

};
struct SkinHeader
{
public:
	SkinHeader()
	{

	}
	int nIndices;
	int ofsIndices;
	int nTriangles;
	int ofsTriangles;
	int nProperties;
	int ofsProperties;
	int nSubmeshes;
	int ofsSubmeshes;
	int nTextureUnits;
	int ofsTextureUnits;
	int LOD;
};

#endif // !STRUCTURES_H
