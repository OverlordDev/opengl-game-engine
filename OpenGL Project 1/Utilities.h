#ifndef UTILITIES_H
#define UTILITIES_H
#include <fstream>
class File
{
public:
	File(std::string filename)
	{
		readIndex = 0;
		result = ReadAllBytes(filename);
	}
private:
	std::vector<char> result;

	static std::vector<char> ReadAllBytes(std::string filename)
	{
		std::ifstream ifs(filename, std::ios::binary | std::ios::ate);
		std::ifstream::pos_type pos = ifs.tellg();

		std::vector<char>  result(pos);

		ifs.seekg(0, std::ios::beg);
		ifs.read(&result[0], pos);

		return result;
	}
public:
	int readIndex;
	//Reading
	int ReadInt8()
	{
		readIndex++;
		return int((unsigned char)result[readIndex - 1]);
	}
	int ReadInt8(int offset)
	{
		return int((unsigned char)result[offset]);
	}
	int ReadInt16()
	{
		readIndex += 2;
		return	int((unsigned char)(result[readIndex - 1]) << 8 |
			(unsigned char)(result[readIndex - 2]));

	}
	int ReadInt16(int offset)
	{
		return	int((unsigned char)(result[offset + 1]) << 8 |
			(unsigned char)(result[offset]));
	}
	int ReadInt32()
	{
		readIndex += 4;
		return int((unsigned char)(result[readIndex - 1]) << 24 |
			(unsigned char)(result[readIndex - 2]) << 16 |
			(unsigned char)(result[readIndex - 3]) << 8 |
			(unsigned char)(result[readIndex - 4]));

	}
	int ReadInt32(int offset)
	{
		return int((unsigned char)(result[offset + 3]) << 24 |
			(unsigned char)(result[offset + 2]) << 16 |
			(unsigned char)(result[offset + 1]) << 8 |
			(unsigned char)(result[offset]));
	}
	char ReadChar()
	{
		readIndex++;
		return (result[readIndex - 1] < 128 && result[readIndex - 1] > 32) ? result[readIndex - 1] : '.';
	}
	char ReadChar(int offset)
	{
		return (result[offset] < 128 && result[offset] > 32) ? result[offset] : '.';
	}
	float ReadFloat()
	{
		readIndex += 4;
		float output;
		*((unsigned char*)(&output) + 3) = result[readIndex - 1];
		*((unsigned char*)(&output) + 2) = result[readIndex - 2];
		*((unsigned char*)(&output) + 1) = result[readIndex - 3];
		*((unsigned char*)(&output) + 0) = result[readIndex - 4];
		return output;

	}
	float ReadFloat(int offset)
	{
		float output;
		*((unsigned char*)(&output) + 3) = result[offset + 3];
		*((unsigned char*)(&output) + 2) = result[offset + 2];
		*((unsigned char*)(&output) + 1) = result[offset + 1];
		*((unsigned char*)(&output) + 0) = result[offset];
		return output;

	}

	//Writing ( not yet implemented ) 
};


#endif
