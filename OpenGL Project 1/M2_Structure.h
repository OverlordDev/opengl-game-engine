#pragma once

#include <glm.hpp>
#include <string>
#include <vector>
#include "Utilities.h"
#include "Structures.h"
class M2Model
{
	//
	typedef unsigned int uint;
	typedef unsigned short ushort;

public:
	M2Model(std::string fileName)
	{
		File* file = new File(fileName);

		//MAGIC
		MAGIC[0] = file->ReadChar();
		MAGIC[1] = file->ReadChar();
		MAGIC[2] = file->ReadChar();
		MAGIC[3] = file->ReadChar();


		if (strcmp(MAGIC, "MD20") == 0) // valid format for M2
		{
			//Continue with the rest
			IVersion = file->ReadInt32();
			IName = file->ReadInt32();
			ofsName = file->ReadInt32();
			type = E_GLOBALFLAGS(file->ReadInt32());
			nGlobalSequences = file->ReadInt32();
			ofsGlovalSequences = file->ReadInt32();
			nAnimations = file->ReadInt32();
			ofsAnimations = file->ReadInt32();
			nAnimationLookup = file->ReadInt32();
			ofsAnimationLookup = file->ReadInt32();
			nBones = file->ReadInt32();
			ofsBones = file->ReadInt32();
			nKeyBoneLookup = file->ReadInt32();
			ofsKeyBoneLookup = file->ReadInt32();
			nVertices = file->ReadInt32();
			ofsVertices = file->ReadInt32();
			nViews = file->ReadInt32();
			nColors = file->ReadInt32();
			ofsColors = file->ReadInt32();
			nTextures = file->ReadInt32();
			ofsTextures = file->ReadInt32();
			nTransparency = file->ReadInt32();
			ofsTransparency = file->ReadInt32();
			nTextureanimations = file->ReadInt32();
			ofsTextureanimations = file->ReadInt32();
			nTexReplace = file->ReadInt32();
			ofsTexReplace = file->ReadInt32();
			nRenderFlags = file->ReadInt32();
			ofsRenderFlags = file->ReadInt32();
			nBoneLookupTable = file->ReadInt32();
			ofsBoneLookupTable = file->ReadInt32();
			nTexLookup = file->ReadInt32();
			ofsTexLookup = file->ReadInt32();
			nTexUnits = file->ReadInt32();
			ofsTexUnits = file->ReadInt32();
			nTransLookup = file->ReadInt32();
			ofsTransLookup = file->ReadInt32();
			nTexAnimLookup = file->ReadInt32();
			ofsTexAnimLookup = file->ReadInt32();

			//TheFloats - a bit messy
			float x, y, z;
			x = file->ReadFloat();
			y = file->ReadFloat();
			z = file->ReadFloat();
			theFloats.VertexBoxMin = glm::vec3(x, y, z);
			x = file->ReadFloat();
			y = file->ReadFloat();
			z = file->ReadFloat();
			theFloats.VertexBoxMax = glm::vec3(x, y, z);
			theFloats.VertexBoxRadius = file->ReadFloat();
			x = file->ReadFloat();
			y = file->ReadFloat();
			z = file->ReadFloat();
			theFloats.BoundingBoxMin = glm::vec3(x, y, z);
			x = file->ReadFloat();
			y = file->ReadFloat();
			z = file->ReadFloat();
			theFloats.BoundingBoxMin = glm::vec3(x, y, z);
			theFloats.BoundingBoxRadius = file->ReadFloat();
			// End of the floats
			for (int i = 0; i < IName - 1; i++)
			{
				ModelName += file->ReadChar(ofsName + i);
			}
			//Vertices
			file->readIndex = ofsVertices;
			for (int i = 0; i < nVertices; i++)
			{
				M2Vertex vertex;
				vertex.Position.x = file->ReadFloat();
				vertex.Position.y = file->ReadFloat();
				vertex.Position.z = file->ReadFloat();

				vertex.BoneWeight.x = file->ReadInt8();
				vertex.BoneWeight.y = file->ReadInt8();
				vertex.BoneWeight.z = file->ReadInt8();
				vertex.BoneWeight.w = file->ReadInt8();

				vertex.BoneIndices.x = file->ReadInt8();
				vertex.BoneIndices.y = file->ReadInt8();
				vertex.BoneIndices.z = file->ReadInt8();
				vertex.BoneIndices.w = file->ReadInt8();

				vertex.Normals.x = file->ReadFloat();
				vertex.Normals.y = file->ReadFloat();
				vertex.Normals.z = file->ReadFloat();

				vertex.TextureCoords.x = file->ReadFloat();
				vertex.TextureCoords.y = file->ReadFloat();

				vertex.Unknown.x = file->ReadFloat();
				vertex.Unknown.y = file->ReadFloat();

				vertices.push_back(vertex); // add the final vertex to the list
			}
		}

	}

public:
	std::vector<char> result;
	char MAGIC[5] = { '\0' };
	uint IVersion;
	uint IName;
	uint ofsName;
	E_GLOBALFLAGS type;
	uint nGlobalSequences;
	uint ofsGlovalSequences;
	uint nAnimations;
	uint ofsAnimations;
	uint nAnimationLookup;
	uint ofsAnimationLookup;
	uint nBones;
	uint ofsBones;
	uint nKeyBoneLookup;
	uint ofsKeyBoneLookup;
	uint nVertices;
	uint ofsVertices;
	uint nViews;
	uint nColors;
	uint ofsColors;
	uint nTextures;
	uint ofsTextures;
	uint nTransparency;
	uint ofsTransparency;
	uint nTextureanimations;
	uint ofsTextureanimations;
	uint nTexReplace;
	uint ofsTexReplace;
	uint nRenderFlags;
	uint ofsRenderFlags;
	uint nBoneLookupTable;
	uint ofsBoneLookupTable;
	uint nTexLookup;
	uint ofsTexLookup;
	uint nTexUnits;
	uint ofsTexUnits;
	uint nTransLookup;
	uint ofsTransLookup;
	uint nTexAnimLookup;
	uint ofsTexAnimLookup;
	TheFloats theFloats;
	uint nBoundingTriangles;
	uint ofsBoundingTriangles;
	uint nBoundingVertices;
	uint ofsBoundingVertices;
	uint nBoundingNormals;
	uint ofsBoundingNormals;
	uint nAttachments;
	uint ofsAttachments;
	uint nAttachLookup;
	uint ofsAttachLookup;
	uint nEvents;
	uint ofsEvents;
	uint nLights;
	uint ofsLights;
	uint nCameras;
	uint ofsCameras;
	uint nCameraLookup;
	uint ofsCameraLookup;
	uint nRibbonEmitters;
	uint ofsRibbonEmitters;
	uint nParticleEmitters;
	uint ofsParticleEmitters;
	std::string ModelName;
	std::vector<M2Vertex> vertices;
};
class M2Skin
{
public:
	M2Skin(std::string filename)
	{
		File* file = new File(filename);
		MAGIC[0] = file->ReadChar();
		MAGIC[1] = file->ReadChar();
		MAGIC[2] = file->ReadChar();
		MAGIC[3] = file->ReadChar();

		if (strcmp(MAGIC, "SKIN") == 0) // valid format for SKIN
		{
			// Initialize the header struct
			skinHeader.nIndices = file->ReadInt32();
			skinHeader.ofsIndices = file->ReadInt32();
			skinHeader.nTriangles = file->ReadInt32();
			skinHeader.ofsTriangles = file->ReadInt32();
			skinHeader.nProperties = file->ReadInt32();
			skinHeader.ofsProperties = file->ReadInt32();
			skinHeader.nSubmeshes = file->ReadInt32();
			skinHeader.ofsSubmeshes = file->ReadInt32();
			skinHeader.nTextureUnits = file->ReadInt32();
			skinHeader.ofsTextureUnits = file->ReadInt32();
			skinHeader.LOD = file->ReadInt32();

			file->readIndex = skinHeader.ofsTriangles;
			for (int i = 0; i < skinHeader.nTriangles / 3; i++)
			{
				int x = file->ReadInt16();
				int y = file->ReadInt16();
				int z = file->ReadInt16();

				TRITriangles.push_back(glm::ivec3(x, y, z));
			}
		}

	}
public:
	char MAGIC[5] = { '\0' };
	SkinHeader skinHeader;
	std::vector<glm::ivec3> TRITriangles;

};