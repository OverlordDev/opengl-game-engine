#include "Game.h"

std::vector<Vertex> vertices;						
std::vector<GLuint> indices;

unsigned nrOfIndices;

int main()
{
	Game game("Model Editor", 640, 480, 4, 4, true);

	////Model
	//M2Model model("Models//Sword_2H_Ashbringer.m2");
	//M2Skin skin("Models//Sword_2H_Ashbringer00.skin");
	//
	//for (int i = 0; i < model.nVertices; i++)
	//{
	//	Vertex v;
	//	v.position = glm::vec3(model.vertices[i].Position.x, model.vertices[i].Position.y, model.vertices[i].Position.z);
	//	v.color = glm::vec3(1.f, 1.f, 1.f);
	//	v.normal = glm::vec3(model.vertices[i].Normals.x, model.vertices[i].Normals.y, model.vertices[i].Normals.z);
	//	v.texcoord = glm::vec2(model.vertices[i].TextureCoords.x, model.vertices[i].TextureCoords.y);
	//	vertices.push_back(v);
	//}

	//for (int i = 0; i < skin.skinHeader.nTriangles / 3; i++)
	//{
	//	indices.push_back(skin.TRITriangles[i].x);
	//	indices.push_back(skin.TRITriangles[i].y);
	//	indices.push_back(skin.TRITriangles[i].z);
	//}
	//nrOfIndices = indices.size();

	while (!game.getWindowShouldClose())
	{
		//UPDATE INPUT
		game.update();
		game.render();
	}

	return 0;
}
